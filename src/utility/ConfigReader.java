package utility;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ConfigReader {
	Properties pro;

	public ConfigReader()
	{
		try {
			File source = new File ("./configproperties");

			FileInputStream input = new FileInputStream(source);

			pro = new Properties();

			pro.load(input);

		} catch (Exception exp) {

			System.out.println("Exception is: ---"+exp.getMessage());
		} 
	}
	
	
	
	public String getCarzukaURLDev()
	{
		String path = pro.getProperty("CarzukaDev");

		return path;
	}
	
	public String getCarzukaURLTest()
	{
		String path = pro.getProperty("CarzukaTest");

		return path;
	}
	
	public String getCarzukaURLProd()
	{
		String path = pro.getProperty("CarzukaProd");

		return path;
	}
	
	public String getPrivateSellerEmailAddressDev()
	{
		return pro.getProperty("PrivateSellerEmailAddressDev");
	}
	
	public String getPrivateSellerPasswordDev()
	{
		return pro.getProperty("PrivateSellerPasswordDev");
	}
	
	public String getPrivateSellerEmailAddressTest()
	{
		return pro.getProperty("PrivateSellerEmailAddressTest");
	}
	
	public String getPrivateSellerPasswordTest()
	{
		return pro.getProperty("PrivateSellerPasswordTest");
	}
	
	public String getPrivateSellerEmailAddressProd()
	{
		return pro.getProperty("PrivateSellerEmailAddressProd");
	}
	
	public String getPrivateSellerPasswordProd()
	{
		return pro.getProperty("PrivateSellerPasswordProd");
	}

	
	public String getBrowser()
	{
		return pro.getProperty("activebrowser");

	}
	
	public String getPlatform()
	{
		return pro.getProperty("activeplatform");
	}
	
	public String getEnvironmentToUse()
	{
		return pro.getProperty("activesite");
	}
	
	public String getTestBed()
	{
		if(Constant.testBed.contains("local")) {
			return "local";
		}else {
			return pro.getProperty("activetestbed");
		}
	}
	
	public String getGrid_IP_Port()
	{
		return pro.getProperty("grid_ip_port");
	}
	
}