package utility;

import static org.testng.Assert.assertTrue;
import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.testng.annotations.Optional;

import pageObject.HomePage;
import pageObject.LoginPage;
import pageObject.SellMyCar;


public class DoThis {

	public static void InitElements() {
		PageFactory.initElements(Constant.driver, LoginPage.class);
		PageFactory.initElements(Constant.driver, HomePage.class);
		PageFactory.initElements(Constant.driver, SellMyCar.class);
		
	}


	public static void click(WebElement webElement, @Optional("Y") String ScreenCapture) throws InterruptedException {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		String ObjectName = "";
		try {
			WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
			wait.until(ExpectedConditions.elementToBeClickable(webElement));
			PageUtil.scrolltoElement(webElement);
			ObjectName = webElement.getText();
		} catch (Exception e) {
			Reporter.ReportEventCustomStatus("Unable to perform click to " + webElement.toString(), "warn", "Y");
			
		}

		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("value").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("title").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("innerText").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("aria-label").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("name").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("for").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		
		
		try {
			webElement.click();
		} catch (Exception e) {
//			Reporter.ReportEventCustomStatus("Unable to perform click to " + webElement.toString(), "warn", "Y");
//			Constant.error = true;
//			throw new AssertionError(webElement.toString());
			JavascriptExecutor executor = (JavascriptExecutor)Constant.driver;
			executor.executeScript("arguments[0].click();", webElement);
		}

		if (ScreenCapture.equalsIgnoreCase("Y")) {
			Reporter.attachedImage("Click the [" + ObjectName + "] button");
		} else if (ScreenCapture.equalsIgnoreCase("N")) {
			Reporter.ReportEventNoImage("Click the [" + ObjectName + "] button");
		}

	}

	public static void sendkeys(WebElement webElement, String keys, @Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
		PageUtil.scrolltoElement(webElement);
		// webElement.click();
		webElement.sendKeys(keys);

		String ObjectName = "null";
		try {
			ObjectName = webElement.getAttribute("placeholder").toString();
		} catch (Exception e) {
			ObjectName = "";
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("text").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("aria-label").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("name").toString();
			} catch (Exception e) {
				ObjectName = "WebElement";
			}
		}
		
		if (ScreenCapture.equalsIgnoreCase("Y")) {
			Reporter.attachedImage("Enter '" + keys + "' to {" + ObjectName + "} field");
		} else if (ScreenCapture.equalsIgnoreCase("N")) {
			Reporter.ReportEventNoImage("Enter '" + keys + "' to {" + ObjectName + "} field");
		}

	}

	public static void select(WebElement webElement, String value, @Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
		PageUtil.scrolltoElement(webElement);
		Select select = new Select(webElement);
		select.selectByVisibleText(value);

		// String ObjectName = webElement.getAttribute("aria-label");
		// if(ObjectName==""||ObjectName=="null") {
		// ObjectName = webElement.getAttribute("name");
		// }

		String ObjectName = "null";
		try {
			ObjectName = webElement.getAttribute("placeholder").toString();
		} catch (Exception e) {
			ObjectName = "";
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("text").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("aria-label").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("name").toString();
			} catch (Exception e) {
				ObjectName = "WebElement";
			}
		}

		if (ScreenCapture.equalsIgnoreCase("Y")) {
			Reporter.attachedImage("Selected '" + value + "' from {" + ObjectName + "} dropdown");
		} else if (ScreenCapture.equalsIgnoreCase("N")) {
			Reporter.ReportEventNoImage("Selected '" + value + "' from {" + ObjectName + "} dropdown");
		}

	}

	public static void selectByValue(WebElement webElement, String value, @Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
		PageUtil.scrolltoElement(webElement);
		Select select = new Select(webElement);
		select.selectByValue(value);

		// String ObjectName = webElement.getAttribute("aria-label");
		// if(ObjectName==""||ObjectName=="null") {
		// ObjectName = webElement.getAttribute("name");
		// }

		String ObjectName = "null";
		try {
			ObjectName = webElement.getAttribute("placeholder").toString();
		} catch (Exception e) {
			ObjectName = "";
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("text").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("aria-label").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("name").toString();
			} catch (Exception e) {
				ObjectName = "WebElement";
			}
		}

		if (ScreenCapture.equalsIgnoreCase("Y")) {
			Reporter.attachedImage("Selected '" + value + "' from {" + ObjectName + "} dropdown");
		} else if (ScreenCapture.equalsIgnoreCase("N")) {
			Reporter.ReportEventNoImage("Selected '" + value + "' from {" + ObjectName + "} dropdown");
		}

	}

	public static boolean isObjectDisplayed(WebElement webElement, String ObjectName,
			@Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		boolean objectdisplayed;
		if (ScreenCapture.equalsIgnoreCase("NA")) {
			Constant.WaitTime = false;
		}
		if (!Constant.WaitTime) {
			Constant.WaitTime = true;
			Constant.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				if (webElement.isDisplayed()) {
					objectdisplayed = true;
				} else {
					objectdisplayed = false;
				}
			} catch (Exception e) {
				objectdisplayed = false;
			}
		} else {
			try {
				WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
				wait.until(ExpectedConditions.visibilityOf(webElement));
				if (webElement.isDisplayed()) {
					objectdisplayed = true;
					PageUtil.scrolltoElement(webElement);
				} else {
					objectdisplayed = false;
				}
			} catch (Exception e) {
				objectdisplayed = false;
			}
		}
		if (objectdisplayed) {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImage("The {" + ObjectName + "} is displayed");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("The {" + ObjectName + "} is displayed");
			}

		} else {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImageFailed("The {" + ObjectName + "} is NOT displayed");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("The {" + ObjectName + "} is NOT displayed");
			}
		}

		return objectdisplayed;
	}

	public static boolean isDisplayed(WebElement webElement, String ObjectName) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		boolean objectdisplayed;

		try {
			WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
			wait.until(ExpectedConditions.visibilityOf(webElement));
			if (webElement.isDisplayed()) {
				objectdisplayed = true;
				PageUtil.scrolltoElement(webElement);
			} else {
				objectdisplayed = false;
			}
		} catch (Exception e) {
			objectdisplayed = false;
		}

		return objectdisplayed;
	}

	public static void clear(WebElement webElement, @Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
		// PageUtil.scrolltoElement(webElement);
		webElement.clear();

		// String ObjectName = webElement.getAttribute("placeholder");
		// if(ObjectName==null) {
		// ObjectName = webElement.getAttribute("text");
		// // System.out.println("null "+ObjectName);
		// }else if(ObjectName.isEmpty()) {
		// ObjectName = webElement.getAttribute("aria-label");
		// // System.out.println("empty "+ObjectName);
		// }

		String ObjectName = "null";
		try {
			ObjectName = webElement.getAttribute("placeholder").toString();
		} catch (Exception e) {
			ObjectName = "";
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("text").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("aria-label").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("name").toString();
			} catch (Exception e) {
				ObjectName = "WebElement";
			}
		}
		
		if (ScreenCapture.equalsIgnoreCase("Y")) {
			Reporter.attachedImage("{" + ObjectName + "} is cleared with value");
		} else if (ScreenCapture.equalsIgnoreCase("N")) {
			Reporter.ReportEventNoImage("{" + ObjectName + "} is cleared with value");
		}

	}

	public static void keyPress(Keys keystroke) throws InterruptedException {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);

		Actions action = new Actions(Constant.driver);
		action.sendKeys(keystroke).build().perform();
		Thread.sleep(1000);
		Reporter.ReportEventNoImage("[" + keystroke.name() + "] key is pressed.");

	}

	public static boolean isContainingText(WebElement webElement, String TextString, String ObjectName,
			@Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		boolean containtext = false;
		if (ScreenCapture.equalsIgnoreCase("NA")) {
			Constant.WaitTime = false;
		}
		if (!Constant.WaitTime) {
			Constant.WaitTime = true;
			Constant.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				System.out.println(webElement.getText() + " -- " + TextString);
				if (webElement.getText().contains((TextString))) {
					containtext = true;
					PageUtil.scrolltoElement(webElement);
				} else {
					containtext = false;
				}
			} catch (Exception e) {
				containtext = false;
			}

		} else {
			try {
				wait.until(ExpectedConditions.elementToBeClickable(webElement));
				containtext = webElement.getText().toLowerCase().contains((TextString.toLowerCase()));
				// System.out.println(webElement.getText().toLowerCase().contains((TextString.toLowerCase())));
				PageUtil.scrolltoElement(webElement);
			} catch (Exception e) {
				containtext = false;
			}
		}
		if (containtext) {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImage("{" + ObjectName + "} string " + webElement.getText() + " contains '"
						+ TextString + "' string");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("{" + ObjectName + "} string " + webElement.getText() + " contains '"
						+ TextString + "' string");
			}
			return true;
		} else {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImageFailed("{" + ObjectName + "} does not contain '" + TextString + "' string");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("{" + ObjectName + "} does not contain '" + TextString + "' string");
			}
			return false;
		}

	}

	public static boolean isContainingValue(WebElement webElement, String Value, String ObjectName,
			@Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		boolean containtext = false;
		if (ScreenCapture.equalsIgnoreCase("NA")) {
			Constant.WaitTime = false;
		}
		if (!Constant.WaitTime) {
			Constant.WaitTime = true;
			Constant.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				if (webElement.getAttribute("value").contains((Value))) {
					containtext = true;
					PageUtil.scrolltoElement(webElement);
				} else {
					containtext = false;
				}
			} catch (Exception e) {
				containtext = false;
			}
		} else {
			try {
				containtext = webElement.getAttribute("value").toLowerCase().contains((Value.toLowerCase()));
				// System.out.println(webElement.getText().toLowerCase().contains((TextString.toLowerCase())));
				wait.until(ExpectedConditions.elementToBeClickable(webElement));
				PageUtil.scrolltoElement(webElement);
			} catch (Exception e) {
				containtext = false;
			}
		}
		if (containtext) {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImage("{" + ObjectName + "} value " + webElement.getAttribute("value") + " contains '"
						+ Value + "' string");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("{" + ObjectName + "} value " + webElement.getAttribute("value")
						+ " contains '" + Value + "' string");
			}
			return true;
		} else {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImageFailed("{" + ObjectName + "} does not contain '" + Value + "' string");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("{" + ObjectName + "} does not contain '" + Value + "' string");
			}
			return false;
		}

	}

	public static boolean isEqualText(WebElement webElement, String TextString, String ObjectName,
			@Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		String ElementText = "";
		boolean Skip = false;
		try {
			Skip = false;
			wait.until(ExpectedConditions.elementToBeClickable(webElement));
			ElementText = webElement.getText();
			PageUtil.scrolltoElement(webElement);
			if (ElementText == "" || ElementText == " " || ElementText.isEmpty()) {
				ElementText = webElement.getAttribute("value");
				ElementText = ElementText.toString();
				// System.out.println("getText return nothing but got this -- "
				// +ElementText);
			}
		} catch (Exception e) {
			Reporter.ReportEventCustomStatus("The element " + webElement + "is not displayed", "warn", "Y");
			Skip = true;
		}
		if (Skip == false) {
			if (ElementText.equalsIgnoreCase(TextString)) {
				if (ScreenCapture.equalsIgnoreCase("Y")) {
					Reporter.attachedImage("{" + ObjectName + "} text captured  " + ElementText + " is EQUAL to '"
							+ TextString + "' string");
				} else if (ScreenCapture.equalsIgnoreCase("N")) {
					Reporter.ReportEventNoImage("{" + ObjectName + "} text captured " + ElementText + " is EQUAL to '"
							+ TextString + "' string");
				}
				return true;
			} else {
				if (ScreenCapture.equalsIgnoreCase("Y")) {
					Reporter.attachedImageFailed("{" + ObjectName + "} text captured " + ElementText
							+ " is NOT equal to '" + TextString + "' string");
				} else if (ScreenCapture.equalsIgnoreCase("N")) {
					Reporter.ReportEventNoImage("{" + ObjectName + "} text captured " + ElementText
							+ " is NOT equal to '" + TextString + "' string");
				}
				return false;
			}
		} else {
			return false;
		}

	}

	public static boolean isEqualTextSelected(WebElement webElement, String TextString, String ObjectName,
			@Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		String ElementText = "";
		try {
			wait.until(ExpectedConditions.elementToBeClickable(webElement));
			Select select = new Select(webElement);
			WebElement selected = select.getFirstSelectedOption();
			ElementText = selected.getText();

			PageUtil.scrolltoElement(webElement);
			if (ElementText == "" || ElementText == " " || ElementText.isEmpty()) {
				ElementText = webElement.getAttribute("value");
				ElementText = ElementText.toString();
				// System.out.println("getText return nothing but got this -- "
				// +ElementText);
			}
		} catch (Exception e) {
			Reporter.ReportEventCustomStatus("The element " + webElement + "is not displayed", "warn", "Y");
		}

		if (ElementText.equalsIgnoreCase(TextString)) {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImage("{" + ObjectName + "} text captured " + ElementText + " is EQUAL to '"
						+ TextString + "' string");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("{" + ObjectName + "} text captured " + ElementText + " is EQUAL to '"
						+ TextString + "' string");
			}
			return true;
		} else {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImageFailed("{" + ObjectName + "} text captured " + ElementText + " is NOT equal to '"
						+ TextString + "' string");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("{" + ObjectName + "} text captured " + ElementText + " is NOT equal to '"
						+ TextString + "' string");
			}
			return false;
		}

	}

	public static boolean isSelectedContainingText(WebElement webElement, String TextString, String ObjectName,
			@Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		String ElementText = "";
		try {
			wait.until(ExpectedConditions.elementToBeClickable(webElement));
			Select select = new Select(webElement);
			WebElement selected = select.getFirstSelectedOption();
			ElementText = selected.getText();

			PageUtil.scrolltoElement(webElement);
			if (ElementText == "" || ElementText == " " || ElementText.isEmpty()) {
				ElementText = webElement.getAttribute("value");
				ElementText = ElementText.toString();
				// System.out.println("getText return nothing but got this -- "
				// +ElementText);
			}
		} catch (Exception e) {
			Reporter.ReportEventCustomStatus("The element " + webElement + "is not displayed", "warn", "Y");
		}

		if (ElementText.contains(TextString)) {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImage("{" + ObjectName + "} text captured " + ElementText + " is containing '"
						+ TextString + "' string");
			} 
			if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("{" + ObjectName + "} text captured " + ElementText + " is containing '"
						+ TextString + "' string");
			}
			return true;
		} else {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImageFailed("{" + ObjectName + "} text captured " + ElementText + " does NOT contain '"
						+ TextString + "' string");
			} 
			if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("{" + ObjectName + "} text captured " + ElementText + " does NOT contain '"
						+ TextString + "' string");
			}
			return false;
		}

	}

	public static void selectRadio(By by, String selection, @Optional("Y") String ScreenCapture) {

		try {
			List<WebElement> radio = Constant.driver.findElements(by);

			for (int i = 0; i <= radio.size(); i++) {
				// System.out.println(radio.get(i).getText());
				if (radio.get(i).getText().contains(selection)) {
					DoThis.click(radio.get(i), "Y");
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("No Radio button with the selection found -- " + selection);
		}

	}

	public static void selectCombo(By by, String selection, @Optional("Y") String ScreenCapture) {

		try {
			List<WebElement> radio = Constant.driver.findElements(by);

			for (int i = 0; i <= radio.size(); i++) {
				// System.out.println(radio.get(i).getText());
				if (radio.get(i).getText().contains(selection)) {
					DoThis.click(radio.get(i), "Y");
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("No  selection found -- " + selection);
		}

	}

	public static void customSelectDropdown(WebElement iframe, String selection) throws InterruptedException {

		Constant.driver.switchTo().frame(iframe);
		DoThis.selectCombo(By.xpath("//html/body/div/ul/li"), selection, "Y");
		Constant.driver.switchTo().defaultContent();

	}

	public static String getAttribute(WebElement webElement, String AttributeName) {

		try {
			Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
			WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
			wait.until(ExpectedConditions.visibilityOf(webElement));
		} catch (Exception e) {
		}
		PageUtil.scrolltoElement(webElement);
		String AttributeValue = webElement.getAttribute(AttributeName);
		System.out.println(AttributeValue + "--getattribute");
		return AttributeValue;

	}

	public static String getText(WebElement webElement) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		Constant.driver.manage().timeouts().pageLoadTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		Constant.driver.manage().timeouts().implicitlyWait(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
		// PageUtil.scrolltoElement(webElement);

		// System.out.println(AttributeValue);
		return webElement.getText();

	}

	public static String getSwitchStatus(WebElement webElement) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		PageUtil.scrolltoElement(webElement);

		if (webElement.getAttribute("checked") != null) {
			// System.out.println(webElement.getAttribute("checked")+" kapag
			// true");
			return webElement.getAttribute("checked");

		} else {
			// System.out.println(webElement.getAttribute("checked")+" kapag
			// false");
			return "false";
		}

	}

	public static void uploadFile(WebElement InputObject, String FilePath, @Optional("Y") String ScreenCapture)
			throws InterruptedException {

		ConfigReader config = new ConfigReader();
		if (config.getTestBed().equals("grid")) {
			// System.out.println(InputObject.toString());
			// System.out.println(PageUtil.getXpath(InputObject));
			String Xpath = PageUtil.getXpath(InputObject);
			// System.out.println(Xpath);
			WebElement El = Constant.driver.findElement(By.xpath(Xpath));
			((RemoteWebElement) El).setFileDetector(new LocalFileDetector());
			El.sendKeys(FilePath);
			Reporter.ReportEventCustomStatus("Uploaded the file from -- " + FilePath, "info", "N");
		} else {
			InputObject.sendKeys(FilePath);
			Reporter.ReportEventCustomStatus("Uploaded the file from -- " + FilePath, "info", "N");
		}

		Thread.sleep(2000);

	}

	public static void uploadFilename(String FilenamePath, @Optional("Y") String ScreenCapture)
			throws FindFailed, HeadlessException, AWTException, IOException, InterruptedException {

		// extract the filename
		String Filename;
		int startchar = FilenamePath.lastIndexOf("\\");
		Filename = FilenamePath.substring(startchar + 1, FilenamePath.length() - 4);

		Filename = Filename + ".exe";
		System.out.println(Filename);
		Thread.sleep(4000);
		Runtime.getRuntime().exec("C:\\Resources\\autoit_exe\\" + Filename);
		Thread.sleep(2000);

		/*
		 * //sikuli dependency due to selenium grid run. tc3,4,5,6,14, 54,71
		 * still dependent Screen screen = new Screen();
		 * 
		 * Pattern filename = new Pattern(System.getProperty("user.dir") +
		 * "\\resources\\Filename.jpg"); Pattern open = new
		 * Pattern(System.getProperty("user.dir") + "\\resources\\Open.jpg");
		 * 
		 * screen.wait(filename, 10); screen.type(filename, FilenamePath);
		 * 
		 * if(ScreenCapture.equalsIgnoreCase("Y")) {
		 * Reporter.attachedDesktopImage("Filename from " +FilenamePath+
		 * " is uploaded"); }else if(ScreenCapture.equalsIgnoreCase("N")){
		 * Reporter.ReportEventNoImage("Filename from " +FilenamePath+
		 * " is uploaded"); }
		 * 
		 * screen.click(open); Thread.sleep(2000);
		 */

	}

	public static String getWebTableRowText(String XPathTable, String TableValue, int ColumnOfTableToGetText) {

		List<WebElement> TotalRowCount = Constant.driver.findElements(By.xpath(XPathTable + "/tr"));
		int RowIndex = 1;
		String RowElementText = "";
		for (WebElement rowElement : TotalRowCount) {
			if (rowElement.getText().contains(TableValue)) {
				Constant.currentLocator = By
						.xpath(XPathTable + "/tr[" + RowIndex + "]/td[" + ColumnOfTableToGetText + "]/a");
				RowElementText = rowElement.getText();
				return RowElementText;
			}
			RowIndex = RowIndex + 1;
		}
		System.out.println("getWebTableRowText -- " + "no value");
		return "no value";
	}

	public static WebElement getWebTableRowElement(String XPathTable, String TableValue,
			int ColumnOfTableToGetElement) {

		try {
			Thread.sleep(2000);
			if (Constant.driver.findElement(By.xpath(XPathTable)).isDisplayed()) {
				List<WebElement> Table = Constant.driver.findElements(By.xpath(XPathTable + "/tr"));
				// System.out.println(Table.size());
				WebElement element;
				for (int i = 1; i <= Table.size(); i++) {
					if (ColumnOfTableToGetElement == 0) {
						Constant.currentLocator = By.xpath(XPathTable + "/tr[" + i + "]");
						element = Constant.driver.findElement(Constant.currentLocator);
						if (element.getText().contains(TableValue)) {
							return element;
						}
					}
					Constant.currentLocator = By
							.xpath(XPathTable + "/tr[" + i + "]/td[" + ColumnOfTableToGetElement + "]/descendant::a"); // remove
																														// /a
					element = Constant.driver.findElement(Constant.currentLocator);
					if (element.getText().contains(TableValue)) {
						return element;
					}
				}
			}
			return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static WebElement getWebTableRowElementFromBottom(String XPathTable, String TableValue,
			int ColumnOfTableToGetElement) {

		try {

			if (Constant.driver.findElement(By.xpath(XPathTable)).isDisplayed()) {
				List<WebElement> Table = Constant.driver.findElements(By.xpath(XPathTable + "/tr"));
				System.out.println(Table.size());
				WebElement element;
				for (int i = Table.size(); i >= 1; i--) {
					if (ColumnOfTableToGetElement == 0) {
						Constant.currentLocator = By.xpath(XPathTable + "/tr[" + i + "]");
						element = Constant.driver.findElement(Constant.currentLocator);
						if (element.getText().contains(TableValue)) {
							return element;
						}
					}
					Constant.currentLocator = By
							.xpath(XPathTable + "/tr[" + i + "]/td[" + ColumnOfTableToGetElement + "]/descendant::a"); // remove
																														// /a
					element = Constant.driver.findElement(Constant.currentLocator);
					if (element.getText().contains(TableValue)) {
						return element;
					}
				}
			}
			return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static WebElement customGetWebTableRowElement(String XPathTable, String TableValue,
			int ColumnOfTableToGetElement, String AppendingXpathReturn) {

		try {

			if (Constant.driver.findElement(By.xpath(XPathTable)).isDisplayed()) {
				List<WebElement> Table = Constant.driver.findElements(By.xpath(XPathTable + "/tr"));
				// System.out.println(Table.size());
				WebElement element;
				for (int i = 1; i <= Table.size(); i++) {

					if (ColumnOfTableToGetElement == 0) {
						Constant.currentLocator = By.xpath(XPathTable + "/tr[" + i + "]");
						element = Constant.driver.findElement(Constant.currentLocator);
						if (element.getText().contains(TableValue)) {
							return element;
						}
					}
					Constant.currentLocator = By.xpath(XPathTable + "/tr[" + i + "]");/// td["
																						/// +
																						/// ColumnOfTableToGetElement
																						/// +"]");
																						/// //remove
																						/// /a
					element = Constant.driver.findElement(Constant.currentLocator);
					// System.out.println(element.getText());
					if (element.getText().contains(TableValue)) {
						if (AppendingXpathReturn.contains("nochild")) {
							Constant.currentLocator = By
									.xpath(XPathTable + "/tr[" + i + "]/td[" + ColumnOfTableToGetElement + "]");
							element = Constant.driver.findElement(Constant.currentLocator);
							return element;
						} else if (AppendingXpathReturn.contains("span")) {
							Constant.currentLocator = By
									.xpath(XPathTable + "/tr[" + i + "]/td[" + ColumnOfTableToGetElement + "]/span");
							element = Constant.driver.findElement(Constant.currentLocator);
							return element;
						}
						Constant.currentLocator = By.xpath(XPathTable + "/tr[" + i + "]/td[" + ColumnOfTableToGetElement
								+ "]/descendant::" + AppendingXpathReturn); // remove
																			// /a
						element = Constant.driver.findElement(Constant.currentLocator);
						return element;
					}

				}
			}
			return null;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void clickAllWebTableRowElement(String XPathTable, String TableValue, int ColumnOfTableToGetElement)
			throws InterruptedException {

		List<WebElement> TotalRowCount = Constant.driver.findElements(By.xpath(XPathTable));
		int RowIndex = 1;
		for (WebElement rowElement : TotalRowCount) {
			if (rowElement.getText().contains(TableValue)) {
				Constant.currentLocator = By
						.xpath(XPathTable + "[" + RowIndex + "]/td[" + ColumnOfTableToGetElement + "]/i");
				DoThis.click(Constant.driver.findElement(Constant.currentLocator), "Y");
			}
			RowIndex = RowIndex + 1;
		}

	}

	public static void checkWebTableContainText(String TableXpath, String TextString) {
		By Locator = By.xpath(TableXpath + "/tr");

		List<WebElement> TableList = Constant.driver.findElements(Locator);
		int RowIndex = 1;
		for (int i = 0; i < TableList.size(); i++) {

			if (!TableList.get(i).getText().isEmpty()) {
				if (!TableList.get(i).getText().contains(TextString)) {
					Constant.currentLocator = By.xpath(TableXpath + "/tr[" + RowIndex + "]");
					Constant.webElement = TableList.get(i);
					try {
						PageUtil.scrolltoElement(Constant.webElement);
					} catch (Exception e) {
					}
					Reporter.attachedImageFailed("Webtable filtering - " + TableList.get(i).getText()
							+ " --does not contain a string-- " + TextString);
					// break;
				} else {
					Constant.currentLocator = By.xpath(TableXpath + "/tr[" + RowIndex + "]");
					Constant.webElement = TableList.get(i);
					try {
						PageUtil.scrolltoElement(Constant.webElement);
					} catch (Exception e) {
					}
					Reporter.attachedImage("Webtable filtering - " + TableList.get(i).getText()
							+ " --contain a string-- " + TextString);
				}
			}

			RowIndex = RowIndex + 1;

		}
	}

	public static void Wait(WebElement elementToWait) {

		WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
		wait.until(ExpectedConditions.elementToBeClickable(elementToWait));
		try {
			PageUtil.scrolltoElement(elementToWait);
		} catch (Exception e) {
		}

	}

	public static void detectSpinners() {

		List<WebElement> spinners = Constant.driver
				.findElements(By.xpath("//section[@id=\"content\"]/descendant::i[@class=\"fa fa-spinner fa-pulse\"]"));
		for (WebElement spinner : spinners) {
			System.out.println(spinner.isDisplayed());
			System.out.println(spinner.getText());
		}

	}

	public static void Hover(WebElement hoverToElement) {

		Actions action = new Actions(Constant.driver);
		try {
			action.moveToElement(hoverToElement).perform();
		} catch (Exception e) {
		}

	}

	public static boolean isItemContainOnList(List<WebElement> List, String objectName) {

		for (WebElement rowElement : List) {
			try {
				WebElement parent = rowElement.findElement(By.xpath(".."));
				PageUtil.scrolltoElement(parent);
				if (rowElement.getText().contains(objectName)) {
					PageUtil.scrolltoElement(rowElement);
					return true;
				}
			} catch (Exception e) {
				return false;
			}
		}

		return false;

	}

	public static boolean isAttributePresent(WebElement webElement, String attribute, @Optional("Y") String ScreenCapture) {
		Boolean result = false;
		String ObjectName = "";
		try {
			String value = webElement.getAttribute(attribute);
			
			if (value != null) {
				result = true;
			}
		} catch (Exception e) {
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("value").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("title").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("innerText").toString();
			} catch (Exception e) {
				ObjectName = "";
			}
		}
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("aria-label").toString();
			} catch (Exception e) {
				ObjectName = "WebElement";
			}
		}
		
		if (ObjectName == "" || ObjectName == "null" || ObjectName.isEmpty()) {
			try {
				ObjectName = webElement.getAttribute("label").toString();
			} catch (Exception e) {
				ObjectName = "WebElement";
			}
		}
		
		if (ScreenCapture.equalsIgnoreCase("Y")) {
			Reporter.attachedImage(ObjectName + " is a " + attribute);
		} else if (ScreenCapture.equalsIgnoreCase("N")) {
			Reporter.ReportEventNoImage(ObjectName + " is a " + attribute);

		}
		return result;

	}

	public static boolean isObjectNotDisplayed(WebElement webElement, String ObjectName,
			@Optional("Y") String ScreenCapture) {

		Constant.driver.manage().timeouts().setScriptTimeout(Constant.ScriptTimeOut, TimeUnit.SECONDS);
		boolean objectnotdisplayed;
		if (ScreenCapture.equalsIgnoreCase("NA")) {
			Constant.WaitTime = false;
		}
		if (!Constant.WaitTime) {
			Constant.WaitTime = true;
			Constant.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try {
				if (webElement.isDisplayed()) {
					objectnotdisplayed = false;
				} else {
					objectnotdisplayed = true;
				}
			} catch (Exception e) {
				objectnotdisplayed = true;
			}
		} else {
			try {
				WebDriverWait wait = new WebDriverWait(Constant.driver, Constant.ScriptTimeOut);
				wait.until(ExpectedConditions.visibilityOf(webElement));
				if (webElement.isDisplayed()) {
					objectnotdisplayed = false;
					PageUtil.scrolltoElement(webElement);
				} else {
					objectnotdisplayed = true;
				}
			} catch (Exception e) {
				objectnotdisplayed = true;
			}
		}
		if (objectnotdisplayed) {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImage("The {" + ObjectName + "} is NOT displayed");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("The {" + ObjectName + "} is NOT displayed");
			}

		} else {
			if (ScreenCapture.equalsIgnoreCase("Y")) {
				Reporter.attachedImageFailed("The {" + ObjectName + "} is displayed");
			} else if (ScreenCapture.equalsIgnoreCase("N")) {
				Reporter.ReportEventNoImage("The {" + ObjectName + "} is displayed");
			}
		}

		return objectnotdisplayed;
	}

	
	public static void isElementListSortedAsc(String Locator) {
		
		ArrayList<String> obtainedList = new ArrayList<>(); 
		List<WebElement> elementList= Constant.driver.findElements(By.xpath(Locator));
		for(WebElement we:elementList){
		   obtainedList.add(we.getText());
		}
		ArrayList<String> sortedList = new ArrayList<>();  
		for(String s:obtainedList){
		sortedList.add(s);
		}
		Collections.sort(sortedList);
		Assert.assertTrue(sortedList.equals(obtainedList));
		
		
		Reporter.ReportEventNoImage("The {" +Locator.toString()+ "} is sorted alphabetically");
		
	}
	
	public static void refreshBrowser() {
		Constant.driver.navigate().refresh();
		Constant.logger.info("Browser was refreshed.");
		Constant.logger.info("Step " + Constant.ReportStepNumber + " - " + "Browser was refreshed.");
		System.out.println("Step " + Constant.ReportStepNumber + " - " + "Browser was refreshed.");
	}


    public static int randomDigit(){
    	Random rand = new Random();
    	int n = rand.nextInt(50) + 1;
           System.out.println("Code generated:"  +n);
           return n;
    }
    
    public static void dragAndDrop(WebElement sourceElement, WebElement targetElement){
    		(new Actions(Constant.driver)).dragAndDrop(sourceElement, targetElement).perform();
    		Reporter.ReportEventNoImage("Source "+sourceElement.getText()+ " moved to Target rank # "+targetElement.getText());
    		
    }
    
    public static void ComparetwoString (String string1, String string2) {
    	if(string1.equals(string2)) {
     		Reporter.ReportEventNoImage(string1.toString()+" is equal to "+string2.toString());
     	}else {
     		Reporter.ReportEventNoImage(string1.toString()+" is NOT equal to "+string2.toString());
     		assertTrue(false);
     	}
    }
    
    public static void WaitForElementToLoad(WebElement webElement)
    {
        WebDriverWait wait = new WebDriverWait(Constant.driver, 60);
        wait.pollingEvery(10, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.visibilityOf(webElement));
        
    }
    
    public static String GetNow() {
    	
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
		return sdf.format(cal.getTime());
    }
    public static String ConvertDate(String dob) throws ParseException {
    	
    	DateFormat formatter = new SimpleDateFormat("ddMMyyyy"); 
    	Date date = (Date)formatter.parse(dob);
    	SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
    	String finalString = newFormat.format(date);
		return finalString;
    }
    
    public static String getCurrentDate() {


    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    	Date date = new Date();
    	String todate = dateFormat.format(date);

    	return todate;

    }
    
   

}
