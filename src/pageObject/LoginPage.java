package pageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

	@FindBy(xpath="//input[@id='email-address-field']")
	public static WebElement txtbxEmailAddress;
	
	@FindBy(xpath="//input[@id='password-field']")
	public static WebElement txtbxPassword;
	
	@FindBy(xpath="//div[text()='Login']/ancestor::button[1]")
	public static WebElement btnLogin;
	
	
}
