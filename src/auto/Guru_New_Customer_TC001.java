package auto;

import static org.testng.Assert.assertTrue;

import java.util.Dictionary;
import java.util.Hashtable;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import actionModule.AddNewCustomer;
import actionModule.LaunchBrowser;
import actionModule.Login;
import pageObject.HomePage;
import utility.Constant;
import utility.DoThis;
import utility.Reporter;


public class Guru_New_Customer_TC001 {

	static ExtentReports extent;
	static ExtentTest logger;

	public static WebDriver driver;

	@BeforeTest
	public void startTest(final ITestContext testContext) throws Exception {
		Constant.TestCasename = testContext.getName();
	}	

	@Parameters("browser")
	@Test
	public static void GURU_NEW_CUSTOMER_TC001(@Optional("chrome") String browser) throws Exception {

		Constant.browser = browser;	
		logger = Reporter.PrepareReport("GURU_NEW_CUSTOMER_TC001");

		LaunchBrowser.Execute();
		
		//Get current date to include as unique identifier
		String CurrentDate = DoThis.GetNow();
		
		Login.Execute();
		
		Dictionary NewCustomerDetails = new Hashtable();
		NewCustomerDetails.put("customer_name", "Tester");
		NewCustomerDetails.put("gender", "female");
		NewCustomerDetails.put("dob", "25121990");
		NewCustomerDetails.put("address", "123 Test Way");
		NewCustomerDetails.put("city", "Cavite");
		NewCustomerDetails.put("state", "Cavite");
		NewCustomerDetails.put("pin", "123456");
		NewCustomerDetails.put("mobile_number", "09121231234");
		NewCustomerDetails.put("email", "eddelacruzjr"+CurrentDate+"@yahoo.com");
		NewCustomerDetails.put("password", "Test1234");
		
		//Create New Customer
		AddNewCustomer.Execute(NewCustomerDetails);
		
		//Validate if Customer Registered Successfully!!! is displayed
		DoThis.isObjectDisplayed(HomePage.Manager.hdrCustomerRegisteredSuccessfully, "Customer Registered Successfully!!! validation", "Y");
		
		//Validate data matched between
		
		//Customer Name validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTableCustomerName, NewCustomerDetails.get("customer_name").toString(), "Customer Name validation", "Y");
		
		//Gender validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTableGender, NewCustomerDetails.get("gender").toString(), "Gender validation", "Y");
		
		//Birthdate validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTableBirthdate, DoThis.ConvertDate(NewCustomerDetails.get("dob").toString()), "Birthdate validation", "Y");
		
		//Address validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTableAddress, NewCustomerDetails.get("address").toString(), "Address validation", "Y");
		
		//City validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTableCity, NewCustomerDetails.get("city").toString(), "City validation", "Y");
		
		//State validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTableState, NewCustomerDetails.get("state").toString(), "State validation", "Y");
		
		//Pin validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTablePin, NewCustomerDetails.get("pin").toString(), "PIN validation", "Y");
		
		//Mobile Number validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTableMobileNo, NewCustomerDetails.get("mobile_number").toString(), "Mobile Number validation", "Y");
		
		//Email validation
		DoThis.isContainingText(HomePage.Manager.objCustomerTableEmail, NewCustomerDetails.get("email").toString(), "Email validation", "Y");
		
		if(Constant.error) {assertTrue(false);}

	}

	@AfterTest
	public static void EndOftest()
	{

		Constant.driver.quit();	


	}

}
