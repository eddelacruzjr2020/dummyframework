package auto;

import static org.testng.Assert.assertTrue;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import actionModule.LaunchBrowser;
import actionModule.Login;
import pageObject.HomePage;
import utility.Constant;
import utility.DoThis;
import utility.Reporter;


public class Carzuka_SellMyCar_TC001 {

	static ExtentReports extent;
	static ExtentTest logger;

	public static WebDriver driver;

	@BeforeTest
	public void startTest(final ITestContext testContext) throws Exception {
		Constant.TestCasename = testContext.getName();
	}	

	@Parameters("browser")
	@Test
	public static void CARZUKABUYANDSELL_SellMyCar_TC001(@Optional("firefox") String browser) throws Exception {

		Constant.browser = browser;	
		logger = Reporter.PrepareReport("CARZUKA BUY AND SELL_Sell My Car_TC001");

		LaunchBrowser.Execute();
		
		Login.PrivateSeller();


		if(Constant.error) {assertTrue(false);}

	}

	@AfterTest
	public static void EndOftest()
	{

		Constant.driver.quit();	


	}

}
