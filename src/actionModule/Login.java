package actionModule;

import java.io.IOException;

import pageObject.HomePage;
import pageObject.LoginPage;
import utility.ConfigReader;
import utility.DoThis;

public class Login{	
	
	public static void PrivateSeller() throws IOException, Exception{

		String Username = null;
		String Password = null;
		
		ConfigReader config = new ConfigReader();
		switch(config.getEnvironmentToUse()) {
			case "Dev":
				Username = config.getPrivateSellerEmailAddressDev();
				Password = config.getPrivateSellerPasswordDev(); break;
			
			case "Test":
				Username = config.getPrivateSellerEmailAddressTest();
				Password = config.getPrivateSellerPasswordTest(); break;
				
			case "Prod":
			
				Username = config.getPrivateSellerEmailAddressProd();
				Password = config.getPrivateSellerPasswordProd(); break;
			}
		
		//Click Login button
		DoThis.click(HomePage.btnLogin, "Y");
			
		//Email Address
		DoThis.clear(LoginPage.txtbxEmailAddress, "N");
		DoThis.sendkeys(LoginPage.txtbxEmailAddress, Username, "Y");
	
		//Password
		DoThis.clear(LoginPage.txtbxPassword, "N");
		DoThis.sendkeys(LoginPage.txtbxPassword, Password, "Y");
		
		//Click Login Button
		DoThis.click(LoginPage.btnLogin, "Y");
		
	}	
	

}
