package actionModule;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.microsoft.edge.seleniumtools.EdgeOptions;
import utility.ConfigReader;
import utility.Constant;
import utility.DoThis;
import utility.Reporter;

public class LaunchBrowser extends utility.PageUtil{


	public static WebDriver driver = null;	
	//private static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
	public static void Execute() throws MalformedURLException{

		//		DOMConfigurator.configure("log4j.xml");
		Constant.consoleLog.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

		//String Browser = Constant.browser;
		ConfigReader config = new ConfigReader();
		String grid_ipport = config.getGrid_IP_Port();
		Constant.testBed=Constant.browser;

		Constant.consoleLog.info("browser-- " +config.getBrowser());
		Constant.consoleLog.info("Platform -- " +config.getPlatform());
		Constant.consoleLog.info("TestBed -- " +config.getTestBed());

		switch(config.getTestBed()) {
		case "local":
			switch(config.getPlatform()) {
			case "win10":
				switch(config.getBrowser()) {
				case "chrome":
					DesiredCapabilities dc = new DesiredCapabilities();
                    dc.setCapability("acceptInsecureCerts",true);
					System.setProperty("webdriver.chrome.driver", Constant.Path_Chromedriver);
					
					driver = new ChromeDriver(dc);
					//driver.set(new ChromeDriver());
					Constant.driver = (WebDriver) driver; break;
				case "ie":
					System.setProperty("webdriver.ie.driver", Constant.Path_IEdriver);
					driver = new InternetExplorerDriver();					  
					Constant.driver = (WebDriver) driver; break;
				case "firefox":
					System.setProperty("webdriver.gecko.driver", Constant.Path_Geckodriver);
					System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
					driver = new FirefoxDriver();					  
					Constant.driver = (WebDriver) driver; break;			
				case "msedge":
					System.setProperty("webdriver.edge.driver", Constant.Path_MsEdgedriver);
					driver = new EdgeDriver();					  
					Constant.driver = (WebDriver) driver; break;
				}
			} break;
		case "headless":
			switch(config.getPlatform()) {
			case "win10":
				switch(config.getBrowser()) {
				case "chrome":
					System.setProperty("webdriver.chrome.driver", Constant.Path_Chromedriver);
					ChromeOptions chromeoptions = new ChromeOptions();
					chromeoptions.addArguments("headless");
					chromeoptions.addArguments("--disable-gpu");
					chromeoptions.addArguments("disable-infobars");
					chromeoptions.addArguments("--disable-extensions");
					chromeoptions.addArguments("window-size=1980x1080");
					driver = new ChromeDriver(chromeoptions); 
					Constant.driver = (WebDriver) driver; break;
				case "firefox":
					System.setProperty("webdriver.gecko.driver", Constant.Path_Geckodriver);
					System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
					FirefoxOptions firefoxoptions = new FirefoxOptions();
					firefoxoptions.addArguments("--headless");
					firefoxoptions.addArguments("--disable-gpu");
					firefoxoptions.addArguments("window-size=1980x1080");
					driver = new FirefoxDriver(firefoxoptions);					  
					Constant.driver = (WebDriver) driver; break;	
				case "msedge":
					System.setProperty("webdriver.edge.driver", Constant.Path_MsEdgedriver);
					EdgeOptions msedgeoptions = new EdgeOptions();
					msedgeoptions.addArguments("headless");
					msedgeoptions.addArguments("disable-gpu");
					msedgeoptions.addArguments("window-size=1980x1080");
					msedgeoptions.addArguments("--no-sandbox");
					driver = new EdgeDriver(msedgeoptions); 
					Constant.driver = (WebDriver) driver; break;
				}break;
			}


		}

		String url = "Test";//default
		if(Constant.ProdDomainURL.isEmpty()) {
			switch(config.getEnvironmentToUse()) {
			case("Dev"): url = config.getCarzukaURLDev();break;
			case("Test"): url = config.getCarzukaURLTest();break;
			case("Prod"): url = config.getCarzukaURLProd();break;
			}
		}else {	url=Constant.ProdDomainURL;	}	

		Constant.driver.manage().timeouts().pageLoadTimeout(30L, TimeUnit.SECONDS);
		Constant.driver.manage().timeouts().implicitlyWait(20L, TimeUnit.SECONDS);
		Constant.driver.get(url);

		Constant.driver.manage().window().maximize();

		Reporter.ReportEventNoImage("Launch Website - " + url);	
		DoThis.InitElements();
	}
	

}
